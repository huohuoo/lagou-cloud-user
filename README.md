# 拉钩用户模块

#### 一、模块介绍
拉钩用户是基于Spring Cloud技术构建的一套互联网用户中心，系统采用微服务架构，实现用户注册、登录为主线，串联验证码生成以及校验、邮件发送、IP防暴刷、用户统一认证等功能。

#### 二、系统业务架构

##### 2.1、业务结构

- 用户注册
  - 验证邮箱是否占用
  - 验证码发送到邮箱
  - 用户注册
  - 频繁注册控制
- 用户登录
  - 登录系统
- 欢迎登录
  - 获取登录者邮箱

##### 2.2、数据库结构

1、验证码表

```sql
CREATE TABLE `lagou_auth_code` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `email` varchar(64) DEFAULT NULL COMMENT '邮箱地址',
  `code` varchar(6) DEFAULT NULL COMMENT '验证码',
  `createtime` datetime DEFAULT NULL COMMENT '创建时间',
  `expiretime` datetime DEFAULT NULL COMMENT '过期时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;
```

2、用户表

```sql
CREATE TABLE `lagou_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `username` varchar(30) DEFAULT NULL COMMENT '用户名',
  `password` varchar(50) DEFAULT NULL COMMENT '密码',
  `email` varchar(64) DEFAULT NULL COMMENT '邮箱',
  `createtime` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
```

3、token表

```sql
CREATE TABLE `lagou_token` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `email` varchar(64) NOT NULL COMMENT '邮箱地址',
  `token` varchar(255) NOT NULL COMMENT '令牌',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
```


#### 三、系统技术架构

![image-20200622101327749](images/image-20200622101327749.png)

功能清单：

| 序号 | 模块                           | 描述                                                |
| :--: | ------------------------------ | --------------------------------------------------- |
|  1   | lagou-users-eureka-server-8761 | 提供注册中心服务，端口8761                          |
|  2   | lagou-users-eureka-server-8762 | 提供注册中心服务，端口8762                          |
|  3   | lagou-service-users-8080       | 提供用户微服务，包含用户注册、用户登录、token等服务 |
|  4   | lagou-service-code-8081        | 提供验证码微服务，包含生成验证码，验证验证码        |
|  5   | lagou-service-email-8082       | 提供邮件发送微服务                                  |
|  7   | lagou-users-configserver-9006  | 提供配置中心服务                                    |
|  8   | lagou-users-gateway-9002       | 提供网关服务                                        |
|  10  | lagou-users-config-repo        | 配置文件仓库                                        |
|  11  | users                          | 静态资源                                            |

配置文件仓库地址：https://gitee.com/lcjtest/lagou-users-config-repo.git

其它内容的仓库地址：https://gitee.com/nicholas_lcj/lagou-cloud-users.git

#### 四、系统详细设计

##### 4.1、注册中心

构建注册中心，分别占用8761、8762端口。本机host配置域名：LagouCloudEurekaServerA和LagouCloudEurekaServerB分别表示eureka的域名。

```xml
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-netflix-eureka-server</artifactId>
</dependency>
```

application.yml，这里列出8761的配置，8762的配置基本相同。

```yml
# eureka server服务端口
server:
  port: 8761
spring:
  application:
    name: lagou-users-eureka-server #应用名称，会在Eureka中作为服务名称

# eureka 客户端配置（和Server交互），Eureka Server其实也是一个Client
eureka:
  instance:
    hostname: LagouCloudEurekaServerA # 当前Eureka实例的主机名
  client:
    service-url: #配置客户端所交互的eureka Server的地址（Eureka Server集群中每一个Server其实相对于其它Server来说都是Client）
      # 集群模式下，defaultZone应该指向其它Eureka Server，如果有更多其它Server实例，逗号拼接即可
      defaultZone: http://LagouCloudEurekaServerB:8762/eureka
    register-with-eureka: true #单例模式：自己就是服务，不需要注册自己，设置为false，集群模式下设置为true
    fetch-registry: true # 查询获取注册中心的服务信息，自己就是Server，不需要查询,默认为true,集群模式式设置为true
```

##### 4.2、配置中心

配置中心需要集成eureka客户端、spring cloud config server 、spring cloud bus。

```xml
 <!-- 1、导入Eureka Client客户端 -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
</dependency>
<!-- 2、导入Spring Cloud Config server-->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-config-server</artifactId>
</dependency>
<!-- 3、导入spring cloud bus-->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-bus-amqp</artifactId>
</dependency>
```

application.yml

```yml
server:
  port: 9006
eureka:
  client:
    service-url:
      # 注册到集群，就把多个EurekaServer地址使用逗号连接起来即可；注册单实例，那就写一个就OK
      defaultZone: http://LagouCloudEurekaServerA:8761/eureka,http://LagouCloudEurekaServerB:8762/eureka
  instance:
    prefer-ip-address: true
    instance-id: ${spring.cloud.client.ip-address}:${spring.application.name}:${server.port}:@project.version@
spring:
  application:
    name: lagou-users-configserver
  cloud:
    config:
      server:
        git:
          uri: https://gitee.com/lcjtest/lagou-users-config-repo.git
          username: lcjtest
          password: lcjtest1234
          search-paths:
            - lagou-users-config-repo
      # 读取分支
      label: master
  # 当然这里springboot已经约定了配置默认本机，可以不写。
  rabbitmq:
    host: 127.0.0.1
    username: guest
    password: guest
    port: 5762
# spring boot 中暴露健康检查等断点接口
management:
  endpoints:
    web:
      exposure:
        include: "*"
  endpoint:
    health:
      show-details: always
```

##### 4.3、用户微服务

用户微服务提供用户注册、登录、获取用户登录信息等接口。用户微服务集成了mybatis、eureka客户端、config客户端、feign客户端、druid数据库连接池等组件，搭建dao、service、controller三层。

dao层采用mybatis访问lagou_users和lagou_token数据库表。

service层分为用户服务以及通过feign客户端调用验证码微服务，实现注册时验证码的验证功能。

controller层包含上面提到的接口信息以及异常处理。此外，在jquery ajax请求后台发起重定向时，jquery并不会处理302状态码，因此，需要在响应头部设置重定向的路径和设置重定向使能标志。

用户微服务通过集成spring cloud config客户端，实现从配置中心加载数据库连接信息。

###### 4.3.1、系统集成

- pom.xml

```xml
<!-- 1、导入eureka客户端-->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
</dependency>
<!--2、导入公共包-->
<dependency>
    <groupId>com.lagou.edu</groupId>
    <artifactId>lagou-users-common</artifactId>
    <version>1.0-SNAPSHOT</version>
</dependency>
<!-- 3、导入Config客户端 -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-config-client</artifactId>
</dependency>
<!-- 4、导入spring cloud bus ，这里接入rabbitMQ-->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-bus-amqp</artifactId>
</dependency>
<!-- 5、导入feign客户端-->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-openfeign</artifactId>
</dependency>

<!-- 5、数据库以及其它依赖-->
<!-- Spring MyBatis -->
<dependency>
    <groupId>org.mybatis.spring.boot</groupId>
    <artifactId>mybatis-spring-boot-starter</artifactId>
    <version>2.1.2</version>
</dependency>
<!-- spring bootPageHelper插件-->
<dependency>
    <groupId>com.github.pagehelper</groupId>
    <artifactId>pagehelper-spring-boot-starter</artifactId>
    <version>1.2.5</version>
</dependency>
<!-- spring boot加载通用Mapper-->
<dependency>
    <groupId>tk.mybatis</groupId>
    <artifactId>mapper-spring-boot-starter</artifactId>
    <version>2.0.3-beta1</version>
</dependency>
<dependency>
    <groupId>mysql</groupId>
    <artifactId>mysql-connector-java</artifactId>
    <scope>runtime</scope>
</dependency>
<!-- Spring boot 加载druid数据库连接池-->
<dependency>
    <groupId>com.alibaba</groupId>
    <artifactId>druid-spring-boot-starter</artifactId>
    <version>1.1.18</version>
</dependency>
```

bootstrap.yml

```yml
server:
  port: 8080
# 开启驼峰命名识别
mybatis:
  configuration:
    map-underscore-to-camel-case: true
  # 配置mybatis别名
  type-aliases-package: com.lagou.edu.users.pojo
  mapper-locations: classpath:/mapper/*Mapper.xml

#配置分页插件
pagehelper:
  helper-dialect: mysql
  reasonable: true

#配置springConfig客户端
spring:
  application:
    name: lagou-service-users
  cloud:
    # config客户端配置，和ConfigServer通信，并告知ConfigServer希望获取的配置信息在哪个文件中
    config:
      name: lagou-service-users #配置文件名称
      profile: dev #后缀名称
      label: master #分支名称
      uri: http://localhost:9006 #configServer配置中心地址
#springBoot中暴露健康检查等端点接口
management:
  endpoints:
    web:
      exposure:
        include: "*"
  #暴露健康检查细节
  endpoint:
    health:
      show-details: always
```

lagou-service-users-dev.yml

```yml
eureka:
  client:
    service-url:
      # 注册到集群，就把多个EurekaServer地址使用逗号连接起来即可；注册单实例，那就写一个就OK
      defaultZone: http://LagouCloudEurekaServerA:8761/eureka,http://LagouCloudEurekaServerB:8762/eureka
  instance:
    prefer-ip-address: true # 服务实例中显示IP，而不是显示主机名（兼容老版本eureka）
    # 实例名称：localhost:lagou-service-users:8080，我们可以自定义它
    instance-id:  ${spring.cloud.client.ip-address}:${spring.application.name}:${server.port}:@project.version@
spring:
  datasource:
    url: jdbc:mysql://192.168.0.223:3306/zdy_mybatis?&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=Asia/Shanghai
    username: root
    password: jxdf19961213
    type: com.alibaba.druid.pool.DruidDataSource

```

###### 4.3.2、服务层

- LagouUserService

```java
public interface LagouUserService {

    /**
     * 保存user信息。
     * @param user
     */
    LagouUser saveUser(LagouUser user);

    /**
     * Get By Id
     *
     * @param id 主键
     * @return
     */
    LagouUser getUserById(Long id);

    /**
     * 根据邮箱获取用户信息
     *
     * @param email 邮箱
     * @return 用户信息
     */
    LagouUser getLagouUserByEmail(String email);

    /**
     * 验证邮箱是否已经注册:这里只做简单的获取统计数据，没有做用户状态控制处理
     *
     * @param email
     * @return
     */
    Integer getCheckEmail(String email);

    /**
     * 保存token信息
     *
     * @param token token
     */
    void saveToken(LagouToken token);

    /**
     * 根据email和密码生成token
     *
     * @param email email
     * @param password 密码
     * @return token字符串
     */
    String generateToken(String email,String password);

    /**
     * 根据token值获取token对象
     *
     * @param token token值
     * @return token对象
     */
    LagouToken getTokenByToken(String token);

    /**
     * 根据邮箱获取token
     * @param email
     * @return
     */
    LagouToken getTokenByEmail(String email);
}
```

- LagouCodeServiceFeignClient验证码

```java
@FeignClient(value = "lagou-service-code",fallback = LagouCodeServiceFallback.class,path = "/code")
public interface LagouCodeServiceFeignClient {
    /**
     * 校验验证码是否正确
     * @param email 邮箱
     * @param code 验证码
     * @return
     */
    @GetMapping("/validate/{email}/{code}")
    Integer validate(@PathVariable(name = "email") String email,
                            @PathVariable(name = "code") String code);
}

```

LagouCodeServiceFeignClient容错LagouCodeServiceFallback，容错时返回-1。

###### 4.3.3、Controller层

- 控制器UserController

```java
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private LagouUserService lagouUserService;

    @Autowired
    private LagouCodeServiceFeignClient lagouCodeService;

    /**
     * 用户注册接口，调用验证码服务判断验证码是否正确是否超时，如果正确则保存用户信息
     *
     * @param email email邮箱
     * @param password 密码
     * @param code 验证码
     * @return true：成功，false：失败
     */
    @GetMapping("/register/{email}/{password}/{code}")
    public Boolean register(@PathVariable(name = "email") String email,
                            @PathVariable(name = "password") String password,
                            @PathVariable(name = "code") String code,
                            HttpServletRequest request,
                            HttpServletResponse response) throws IOException ;

    /**
     * 判断是否注册，这里根据邮箱查询统计数量，如果为0表示未注册，其余表示已注册
     *
     * @param email 邮箱
     * @return true：已经注册，false表示未注册
     */
    @GetMapping("/isRegistered/{email}")
    public Boolean isRegistered(@PathVariable(name = "email") String email);

    /**
     * 用户登录
     * 1、验证密码合法性
     * 2、根据用户名和密码生成token
     * 3、并且将token存入数据库
     * 4、写入cookie中
     * 5、返回邮箱地址，重定向到欢迎页
     *
     * @param email
     * @param password
     */
    @GetMapping("/login/{email}/{password}")
    public String login(@PathVariable(name = "email") String email,
                      @PathVariable(name = "password") String password,
                        HttpServletRequest request,
                        HttpServletResponse response) throws IOException;

    /**
     * 根据token查询用户登录邮箱接口
     * @param token
     * @return 邮箱
     */
    @GetMapping("/info/{token}")
    public String info(@PathVariable("token") String token);

}
```

- 错误处理

```java
@ControllerAdvice
public class GloabExceptionResolver {

    @ExceptionHandler(value = UserException.class)
    @ResponseBody
    public String userException(UserException ex, HttpServletResponse response){
        response.setStatus(500);
        return ex.getMessage();
    }

    @ExceptionHandler(value = RegisterException.class)
    @ResponseBody
    public String registerException(RegisterException ex , HttpServletResponse response){
        response.setStatus(500);
        return ex.getMessage();
    }

}
```

- 重定向处理

```java
/**
 * 重定向工具，
 * 处理AJAX重定向问题。
 */
@Slf4j
public class RedirecUtil {
    private RedirecUtil(){}

    /**
     *功能描述
     * 重定向处理
     */
    public static void redirect(HttpServletRequest request,HttpServletResponse response, String redirectUrl){
        try{
            //如果是Ajax请求
            if("XMLHttpRequest".equals(request.getHeader("X-Requested-With"))){
                log.debug("ajax redirect");
                sendRedirect(response,redirectUrl);
            }
            //如果是浏览器地址栏请求
            else {
                log.debug("normal redirect ");
                response.sendRedirect(redirectUrl);
            }
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }
    /**
     *功能描述
     * AJAX重定向
     */
    private static void sendRedirect(HttpServletResponse response, String redirectUrl){
        try {
            //这里并不是设置跳转页面，而是将重定向的地址发给前端，让前端执行重定向
            // 设置跳转地址
            response.setHeader("redirectUrl", redirectUrl);
            //设置跳转使能
            response.setHeader("enableRedirect","true");
            response.flushBuffer();
        } catch (IOException ex) {
            log.error("Could not redirect to: " + redirectUrl, ex);
        }
    }
}
```

##### 4.4、验证码微服务

验证码微服务主要负责生产验证码以及校验验证码功能。验证码微服务集成了mybatis、eureka客户端、config客户端、feign客户端、druid数据库连接池等组件，搭建dao、service、controller三层。

dao层采用mybatis访问lagou_code数据库表。

service层包括验证码服务接口和采用feign接口调用邮件发送服务。

controller层包括生成验证码校验验证码的Restful接口。

在调用发送邮件微服务时，发送邮件的时间在20秒左右才能发送完毕，因此，在集成feign客户端时，需要修改调用的超时时间，其中Ribbon设置为30S，在本例重复次数为0次，因此在这里Hystrix超时时间设置为31秒。

###### 4.4.1、系统集成

- pom.xml

```xml
<!-- 1、导入eureka客户端-->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
</dependency>
<!--2、导入公共包-->
<dependency>
    <groupId>com.lagou.edu</groupId>
    <artifactId>lagou-users-common</artifactId>
    <version>1.0-SNAPSHOT</version>
</dependency>
<!-- 3、导入Config客户端 -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-config-client</artifactId>
</dependency>
<!-- 4、导入spring cloud bus ，这里接入rabbitMQ-->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-bus-amqp</artifactId>
</dependency>
<!-- 5、导入openfeign -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-openfeign</artifactId>
</dependency>
<!-- 6、数据库以及其它依赖-->
<!-- Spring MyBatis -->
<dependency>
    <groupId>org.mybatis.spring.boot</groupId>
    <artifactId>mybatis-spring-boot-starter</artifactId>
    <version>2.1.2</version>
</dependency>
<!-- spring bootPageHelper插件-->
<dependency>
    <groupId>com.github.pagehelper</groupId>
    <artifactId>pagehelper-spring-boot-starter</artifactId>
    <version>1.2.5</version>
</dependency>
<!-- spring boot加载通用Mapper-->
<dependency>
    <groupId>tk.mybatis</groupId>
    <artifactId>mapper-spring-boot-starter</artifactId>
    <version>2.0.3-beta1</version>
</dependency>
<dependency>
    <groupId>mysql</groupId>
    <artifactId>mysql-connector-java</artifactId>
    <scope>runtime</scope>
</dependency>
<!-- Spring boot 加载druid数据库连接池-->
<dependency>
    <groupId>com.alibaba</groupId>
    <artifactId>druid-spring-boot-starter</artifactId>
    <version>1.1.18</version>
</dependency>
```

- bootstrap.yml

```yml
server:
  port: 8081
# 开启驼峰命名识别
mybatis:
  configuration:
    map-underscore-to-camel-case: true
  # 配置mybatis别名
  type-aliases-package: com.lagou.edu.users.pojo
  mapper-locations: classpath:/mapper/*Mapper.xml

#配置分页插件
pagehelper:
  helper-dialect: mysql
  reasonable: true


#配置springConfig客户端
spring:
  application:
    name: lagou-service-code
  cloud:
    # config客户端配置，和ConfigServer通信，并告知ConfigServer希望获取的配置信息在哪个文件中
    config:
      name: lagou-service-code #配置文件名称
      profile: dev #后缀名称
      label: master #分支名称
      uri: http://localhost:9006 #configServer配置中心地址


#springBoot中暴露健康检查等端点接口
management:
  endpoints:
    web:
      exposure:
        include: "*"
  #暴露健康检查细节
  endpoint:
    health:
      show-details: always
```

- lagou-service-code-dev.yml

```yml
logging:
  level:
    com.lagou.edu.users.mapper : debug
eureka:
  client:
    service-url:
      # 注册到集群，就把多个EurekaServer地址使用逗号连接起来即可；注册单实例，那就写一个就OK
      defaultZone: http://LagouCloudEurekaServerA:8761/eureka,http://LagouCloudEurekaServerB:8762/eureka
  instance:
    prefer-ip-address: true # 服务实例中显示IP，而不是显示主机名（兼容老版本eureka）
    # 实例名称：localhost:lagou-service-code:8081，我们可以自定义它
    instance-id:  ${spring.cloud.client.ip-address}:${spring.application.name}:${server.port}:@project.version@
spring:
  datasource:
    url: jdbc:mysql://192.168.0.223:3306/zdy_mybatis?&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=Asia/Shanghai
    username: root
    password: jxdf19961213
    type: com.alibaba.druid.pool.DruidDataSource
# 开启feign的熔断功能
feign:
  hystrix:
    enabled: true
lagou-service-email:
  ribbon:
    ConnectTimeout: 2000
    ReadTimeout: 30000
    OkToRetryOnAllOperations: true
    MaxAutoRetries: 0
    MaxAutoRetriesNextServer: 0
hystrix:
  command:
    default:
      execution:
        isolation:
          thread:
            # hystrix 超时时长设置
            timeoutInMilliseconds: 31000
```

###### 4.4.2、服务层

- LagouAuthCodeService

```java
public interface LagouAuthCodeService {
    /**
     * 根据email获取当前时间的验证码
     * @param param email地址
     * @return 验证码
     */
    LagouAuthCode getAuthCodeByEmailAndTime(LagouAuthCodeParam param);

    /**
     * 创建验证码
     * @return 验证码
     */
     void saveNewAuthCode(LagouAuthCode code);
}
```

- LagouEmailFeignClient

```java
/**
 * 邮件服务Feign客户端
 */
@FeignClient(value = "lagou-service-email",fallback = LagouEmailFallback.class,path = "/")
public interface LagouEmailFeignClient {

    @GetMapping("/email/{email}/{code}")
    Boolean email(@PathVariable(name = "email") String email,
                  @PathVariable(name = "code") String code);
}
```

###### 4.4.3、Controller层

```java
@RestController
@RequestMapping("/code")
public class VerificationCodeController {

    @Autowired
    private LagouAuthCodeService lagouAuthCodeService;

    @Autowired
    private LagouEmailFeignClient lagouEmailService;

    /**
     * 生成验证码
     * 1、随机生成4位数字
     * 2、判断该邮箱在指定的时间内是否生成了验证码
     * 4、调用邮件微服务进行邮件发送
     *
     * @param email
     * @return
     */
    @GetMapping("/create/{email}")
    public Boolean create(@PathVariable(name = "email") String email);

    /**
     * 校验验证码是否正确
     * @param email 邮箱
     * @param code 验证码
     * @return
     */
    @GetMapping("/validate/{email}/{code}")
    public Integer validate(@PathVariable(name = "email") String email,
                            @PathVariable(name = "code") String code);

}
```

##### 4.5、邮件微服务

邮件微服务主要负责发送电子邮件接口，集成eureka客户端、config客户端、spring cloud bus、mail、thymeleaf模板等技术，分别实现服务注册到注册中心、从配置中心加载系统配置文件，动态更新邮件配置信息，邮件发送、模板技术生成发送内容等功能。邮件微服务包括service层和controller层。

controller层包括邮件发送的RESTful接口。

service层处理具体邮件发送业务。

###### 4.5.1、系统集成

```xml
<!-- Eureka客户端-->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
</dependency>
<!-- config client-->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-config-client</artifactId>
</dependency>
<!-- 导入spring cloud bus ，这里接入rabbitMQ-->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-bus-amqp</artifactId>
</dependency>
<!-- email -->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-mail</artifactId>
</dependency>
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-thymeleaf</artifactId>
</dependency>
```

- bootstrap.yml

```yml
server:
  port: 8082
spring:
  application:
    name: lagou-service-email
  cloud:
    # config客户端配置，和configserver通信
    config:
      name: lagou-service-email #配置文件名称
      profile: dev #后缀名称
      label: master #分支名称
      uri: http://localhost:9006 #configServer配置中心地址

#springBoot中暴露健康检查等端点接口
management:
  endpoints:
    web:
      exposure:
        include: "*"
  #暴露健康检查细节
  endpoint:
    health:
      show-details: always
```

- lagou-service-email-dev.yml

```yml
eureka:
  client:
    service-url:
      # 注册到集群，就把多个EurekaServer地址使用逗号连接起来即可；注册单实例，那就写一个就OK
      defaultZone: http://LagouCloudEurekaServerA:8761/eureka,http://LagouCloudEurekaServerB:8762/eureka
  instance:
    prefer-ip-address: true # 服务实例中显示IP，而不是显示主机名（兼容老版本eureka）
    # 实例名称：localhost:lagou-service-code:8081，我们可以自定义它
    instance-id:  ${spring.cloud.client.ip-address}:${spring.application.name}:${server.port}:@project.version@
spring:
  mail:
    host: smtp.163.com
    username: nicholas_lcj@163.com
    password: JFKCDHAMGKGGALRQ
```

###### 4.5.2、服务层

```java
public interface MailService {

    /**
     * 发送
     * @param to 目标
     * @param subject 主题
     * @param text 内容
     */
    void send(String to, String subject, String text);

}
```

###### 4.5.3、Controller层

```java
@RestController
public class EmailController {

    @Autowired
    private MailService mailService;

    /**
     * 发送验证码到邮件
     * @param email 邮箱地址
     * @param code 验证码
     * @return true成功，false失败
     */
    @GetMapping("/email/{email}/{code}")
    public Boolean email(@PathVariable(name = "email") String email,
                         @PathVariable(name = "code") String code){
        try {
            mailService.send(email,"拉钩验证码",code);
            return true;
        }catch (Exception e){
            return false;
        }
    }

}
```

##### 4.6、网关服务

网关服务实现了统一路由、IP防刷爆、统一认证等功能。实现统一路由导入gateway组件即可。

IP防暴刷功能是限制单个客户端IP在最近X分钟内请求注册接口不能超过Y次，因此需要实现对X,Y的配置。网关定义全局过滤器，对注册请求的地址进行拦截，通过漏斗算法判定是否能够注册，如果请求不能通过，则返回303错误以及访问过于频繁，ajax需要捕获303错误，并且给出相应提示。

统一认证功能是登录成功时将生成token写入cookie之后重定向到欢迎页面，后续请求到来时，网关定义全局过滤器，对请求路径进行判断，如果需要拦截，则取出token，通过feign客户端调用用户服务验证token是否有效，如果无效则给出401错误，ajax界面需要捕获401错误，并给出相应提示。

因此整个网关需要继承eureka客户端、feign、config客户端、webflux、config bus等技术。

###### 4.6.1、系统集成

- pom.xml

```xml
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-commons</artifactId>
</dependency>
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
</dependency>
<!--引入feign 验证token-->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-openfeign</artifactId>
</dependency>
<!-- GateWay 网关-->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-gateway</artifactId>
</dependency>
<!-- 引入webflux -->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-webflux</artifactId>
</dependency>
<!-- 导入Config客户端 -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-config-client</artifactId>
</dependency>
<!-- 导入spring cloud bus ，这里接入rabbitMQ-->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-bus-amqp</artifactId>
</dependency>
<!-- 日志依赖 -->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-logging</artifactId>
</dependency>
<!--测试依赖-->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-test</artifactId>
    <scope>test</scope>
</dependency>
<!-- lombok工具 -->
<dependency>
    <groupId>org.projectlombok</groupId>
    <artifactId>lombok</artifactId>
    <version>1.18.4</version>
    <scope>provided</scope>
</dependency>
<!--引入Jaxb,开始-->
<dependency>
    <groupId>com.sun.xml.bind</groupId>
    <artifactId>jaxb-core</artifactId>
    <version>2.2.11</version>
</dependency>
<dependency>
    <groupId>javax.xml.bind</groupId>
    <artifactId>jaxb-api</artifactId>
</dependency>
<dependency>
    <groupId>com.sun.xml.bind</groupId>
    <artifactId>jaxb-impl</artifactId>
    <version>2.2.11</version>
</dependency>
<dependency>
    <groupId>org.glassfish.jaxb</groupId>
    <artifactId>jaxb-runtime</artifactId>
    <version>2.2.10-b140310.1920</version>
</dependency>
<dependency>
    <groupId>javax.activation</groupId>
    <artifactId>activation</artifactId>
    <version>1.1.1</version>
</dependency>
<!-- 引入Jaxb，结束 -->

<!--Actuator 可以帮助你监控和管理Spring Boot应用 -->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-actuator</artifactId>
</dependency>
<!-- 热部署 -->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-devtools</artifactId>
    <optional>true</optional>
</dependency>
```

- bootstrap.yml

```yml
server:
  port: 9002
spring:
  application:
    name: lagou-users-gateway
  cloud:
    config:
      uri: http://localhost:9006 #configServer配置中心地址
      profile: dev
      label: master
      name: lagou-users-gateway
    gateway:
      routes: # 路由可以有多个
        - id: service-users-router # 我们自定义的路由ID，保持唯一
          uri: lb://lagou-service-users  # 配置注册中心服务地址
          predicates: # 断言：路由条件，Predicate 接受一个输入参数，返回Boolean结果。该接口默认包含多种默认方法来讲Predicate组成其他负责的逻辑（比如：与、或、非）
            - Path=/api/user/**
          filters:
            - StripPrefix=1
        - id: service-code-router # 我们自定义的路由ID，保持唯一
          uri: lb://lagou-service-code  # 配置注册中心服务地址
          predicates: # 断言：路由条件，Predicate 接受一个输入参数，返回Boolean结果。该接口默认包含多种默认方法来讲Predicate组成其他负责的逻辑（比如：与、或、非）
            - Path=/api/code/**
          filters:
            - StripPrefix=1
eureka:
  client:
    service-url:
      # 注册到集群，就把多个EurekaServer地址使用逗号连接起来即可；注册单实例，那就写一个就OK
      defaultZone: http://LagouCloudEurekaServerA:8761/eureka,http://LagouCloudEurekaServerB:8762/eureka
  instance:
    prefer-ip-address: true # 服务实例中显示IP，而不是显示主机名（兼容老版本eureka）
    # 实例名称：localhost:lagou-service-code:8081，我们可以自定义它
    instance-id:  ${spring.cloud.client.ip-address}:${spring.application.name}:${server.port}:@project.version@
#springBoot中暴露健康检查等端点接口
management:
  endpoints:
    web:
      exposure:
        include: "*"
  #暴露健康检查细节
  endpoint:
    health:
      show-details: always
```

- lagou-users-gateway-dev.yml

```yml
# 开启feign的熔断功能
feign:
  hystrix:
    enabled: true
lagou-service-users:
  ribbon:
    ConnectTimeout: 2000
    ReadTimeout: 2000
    OkToRetryOnAllOperations: true
    MaxAutoRetries: 0
    MaxAutoRetriesNextServer: 0
hystrix:
  command:
    default:
      execution:
        isolation:
          thread:
            # hystrix 超时时长设置
            timeoutInMilliseconds: 2100
interceptorconfig:
  path:
    exclude:
      - /api/user/login/**
      - /api/user/register/**
      - /api/code/create/**
    include:
      - /api/user/**
      - /api/code/**
# 自定义防刷爆过滤器
flush:
  brush:
    limitTimeMinute: 1
    limitCount: 2
```

###### 4.6.2、统一路由

统一路由配置在bootstrap.yml中配置了用户微服务和验证码微服务的路由规则。前端通过nginx转发过来的请求携带了/api/**，因此需要采用前置过滤器StripPrefix将第一个路径截取掉然后转发到具体的微服务。

###### 4.6.3、IP防暴刷

通过定义全局过滤器FlushBrushFilter实现IP防暴刷功能。过滤器实现了拦截参数进行配置，主要包括limitTimeMinute周期（单位为分钟），limitCount访问次数，配置的信息存入FlushBrushPathBean对象中，FlushBrushPathBean是一个RefreshScope注解接口类，因此，可以实现动态参数配置的功能，与此同时，该类实现了漏斗算法进行限流。请求进入网关之后，通过FlushBrushFilter进行拦截，首先取出请求的IP，并且将IP进入漏斗授权，漏斗没有达到指定容量则放行，如果达到了指定容量，则通过response写出303错误信息。

- FlushBrushFilter

```java
/**
 * 防暴刷过滤器
 */
@Component
public class FlushBrushFilter implements GlobalFilter, Ordered {

    @Autowired
    private FlushBrushPathBean flushBrushPathBean;

    private String filterPath = "/api/user/register/**";

    private PathMatcher matcher = new AntPathMatcher();

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();
        ServerHttpResponse response = exchange.getResponse();
        // 获取请求路径
        String path = request.getURI().getPath();

        // 判断请求是否为注册/api/user/register/**
        if(match(filterPath,path)){
            String host = request.getRemoteAddress().getHostString();
            boolean granted = flushBrushPathBean.isGranted(host);
            if (!granted){
                //表示漏斗溢出，抛出303错误
                return oftenHandle(response);
            }
        }

        return chain.filter(exchange);
    }

    private boolean match(String patternUrl, String requestUrl){
        if (StringUtils.isEmpty(patternUrl) || StringUtils.isEmpty(requestUrl)){
            return false;
        }
        return matcher.match(patternUrl, requestUrl);
    }

    private Mono<Void> oftenHandle(ServerHttpResponse response){
        response.setStatusCode(HttpStatus.SEE_OTHER);//状态码，未认证

        // 这里都是使用WebFluxAPI进行写操作。
        String data = "您频繁进行注册，请求被拒绝。";
        DataBuffer wrap = null;
        try {
            response.getHeaders().add("Content-Type","text/plain; charset=utf-8");
            wrap = response.bufferFactory().wrap(data.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return response.writeWith(Mono.just(wrap));
    }

    @Override
    public int getOrder() {
        return -1;
    }
}

```

- FlushBrushPathBean

```java

@Data
@Component
@RefreshScope
public class FlushBrushPathBean {
    private static final int DEFAULT_LIMIT_TIME_MINUTUE = 1;
    private static final int DEFAULT_LIMIT_COUNT = 1000000;
    private long expire = 2 * 60 * 1000 * DEFAULT_LIMIT_TIME_MINUTUE; //清除时间
    /**
     * 防刷爆时间范围,单位分钟
     */
    @Value("${flush.brush.limitTimeMinute}")
    private Integer limitTimeMinute;

    /**
     * 防刷爆次数
     */
    @Value("${flush.brush.limitCount}")
    private Integer limitCount;

    private double rate = (double) DEFAULT_LIMIT_COUNT / (DEFAULT_LIMIT_TIME_MINUTUE * 60);
    private long capacity = DEFAULT_LIMIT_COUNT * 1000; //容量
    private long lastCleanTime; //最后清除时间

    private Map<String,Long> requestCountMap = new HashMap<>(); //记录漏桶装载量
    private Map<String,Long> requestTimeMap = new HashMap<>(); //记录请求时间

    private Lock lock = new ReentrantLock(true);

    public void reflush(){
        if (limitTimeMinute <= 0 || limitCount <= 0) {
            throw new IllegalArgumentException();
        }
        this.capacity = limitCount * 60 * 1000;
        this.rate = (double) limitCount / (limitTimeMinute * 60);
        this.expire = 2 * 60 * 1000 * limitTimeMinute;
    }

    /**
     * 漏桶算法
     * 利用漏桶算法，进行方法调用限流
     */
    public boolean isGranted(String hostAddr){
        try {
            lock.lock();
            reflush();
            long current = System.currentTimeMillis();
            cleanUp(current); // 清除记录
            Long lastRequestTime = requestTimeMap.get(hostAddr);
            long count = 0;
            if (lastRequestTime == null){
                count += 60 * 1000;
                requestTimeMap.put(hostAddr,current);
                requestCountMap.put(hostAddr,count);
                return true;
            } else {
                count = requestCountMap.get(hostAddr);
                long useTime = current - lastRequestTime;
                count -= (useTime) * rate; //漏水
                count = count > 0 ? count : 0;
                requestTimeMap.put(hostAddr , current);
                if (count < capacity){
                    //容量未满,装水
                    count += 60 * 1000;
                    requestCountMap.put(hostAddr , count);
                    return true;
                }else {
                    // 容量已满
                    requestCountMap.put(hostAddr , count);
                    return false;
                }
            }
        }finally {
            lock.unlock();
        }
    }

    private void cleanUp(long current) {
        if (current - lastCleanTime > expire) {
            for (Iterator<Map.Entry<String, Long>> it = requestTimeMap.entrySet().iterator(); it.hasNext();) {
                Map.Entry<String, Long> entry = it.next();
                if (entry.getValue() < current - expire) {
                    it.remove();
                    requestCountMap.remove(entry.getKey());
                }
            }
            lastCleanTime = current;
        }
    }
}
```

###### 4.6.4、统一认证

统一认证需要配置拦截路径以及排除路径，因为login接口和register接口是不需要进行认证的（配置如上interceptorconfig），配置完成之后对应后端的InterceptorPathBean对象。定义全局统一认证过滤器TokenFilter，对请求进行拦截，如果请求在排除集合中（如登录、注册、生成验证码），此时直接通过放行。如果请求地址在拦截集合中，则取出request中的token，并通过feign客户端调用用户微服务，验证token是否有效，如果token无效则通过response写出401错误，如果有效则放行。

- InterceptorPathBean

```java
@Data
@Component
@ConfigurationProperties(prefix = "interceptorconfig.path")
public class InterceptorPathBean {
    /**
     * 需要拦截的路径。
     */
    private List<String> include;

    /**
     * 排除需要拦截的地址
     */
    private List<String> exclude;

}
```

- TokenFilter

```java
/**
 * Token过滤器，如果没有登录，则重定向到登录页面
 */
@Slf4j
@Component
public class TokenFilter implements GlobalFilter, Ordered {

    @Autowired
    private LagouUsersFeignClient lagouUsersFeignClient;

    @Autowired
    private InterceptorPathBean interceptorPathBean;

    private PathMatcher matcher = new AntPathMatcher();

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        // 1、取出request，response对象

        ServerHttpRequest request = exchange.getRequest();
        ServerHttpResponse response = exchange.getResponse();
        // 2、取出url,判断是否需要过滤
        String requestUrl = request.getURI().getPath();
        if (excludeMatch(requestUrl)){
            // 2.1、如果排除范围，则直接放行
            return chain.filter(exchange);
        }

        if (includeMatch(requestUrl)) {
            // 2.2、如果是包含范围，则过滤

            // 过滤时先取出token,然后验证token是否有效，如果无效返回401错误
            // 3、取出request中的token是否已经失效。
            List<HttpCookie> lagou_token = request.getCookies().get("lagou_token");
            if (lagou_token != null && !lagou_token.isEmpty()){
                // 已登录
                HttpCookie httpCookie = lagou_token.get(0);
                String token = httpCookie.getValue();
                log.debug("token is : {}",token);

                String info = lagouUsersFeignClient.info(token);
                if (info != null){
                    //  已登录，放行
                    log.debug("info is : {}",info);
                    return chain.filter(exchange);
                } else {
                    log.debug("token is timeout.");
                    return unAuthorizedHandle(response);
                }
            } else {
                // 未登录
                return unAuthorizedHandle(response);
            }
        }

        return chain.filter(exchange);
    }

    /**
     * 返回未认证请求
     * @param response
     * @return
     */
    private Mono<Void> unAuthorizedHandle(ServerHttpResponse response){
        response.setStatusCode(HttpStatus.UNAUTHORIZED);//状态码，未认证

        // 这里都是使用WebFluxAPI进行写操作。
        String data = "Request be denied!";
        DataBuffer wrap = response.bufferFactory().wrap(data.getBytes());

        return response.writeWith(Mono.just(wrap));
    }

    /**
     * 匹配排除过滤url，排除返回true，否则返回false
     * @return
     */
    private boolean excludeMatch(String requestUrl){
        return urlMatch(interceptorPathBean.getExclude(),requestUrl);
    }

    /**
     * 匹配url是否需要过滤。
     * @param requestUrl
     * @return
     */
    private boolean includeMatch(String requestUrl){
        return urlMatch(interceptorPathBean.getInclude(),requestUrl);
    }

    private boolean urlMatch(List<String> list,String requestUrl){
        if (list == null || list.isEmpty()){
            return false;
        }
        for (String patternUrl : list){
            boolean isMatch = match(patternUrl,requestUrl);
            if (isMatch){
                return true;
            }
        }
        return false;
    }

    private boolean match(String patternUrl, String requestUrl){
        if (StringUtils.isEmpty(patternUrl) || StringUtils.isEmpty(requestUrl)){
            return false;
        }
        return matcher.match(patternUrl, requestUrl);
    }

    @Override
    public int getOrder() {
        return 0;
    }
}
```

##### 4.7、前端

这里我们只列出JavaScript的代码。

###### 4.7.1、用户登录

```javascript
$(document).ready(function () {
        $(document).ajaxComplete(function(e, xhr, settings){
            redirectHandle(xhr);
        });

        function redirectHandle(xhr) {
            //获取后台返回的参数
            var url = xhr.getResponseHeader("redirectUrl");
            console.log("redirectUrl = " + url);
            var enable = xhr.getResponseHeader("enableRedirect");
            if((enable == "true") && (url != "")){
                var win = window;
                while(win != win.top){
                    win = win.top;
                }
                win.location.href = url;
            }
        }

        $("#btnSubmit").click(function () {
            var email = $(".uname").val().trim();
            var password = $(".pword").val().trim();
            if (email == ""){
                alert("请输入email");
                return ;
            }
            if (password == ""){
                alert("请输入密码");
                return ;
            }
            $.ajax({
                url:"../api/user/login/"+email+"/"+password,
                type:"GET",
                success:function(result,status,xhr){
                },
                statusCode: {
                    500:function(s) {
                        alert(s.responseText);
                    }
                }
            });
        });
    })
```

###### 4.7.2、用户注册

```javascript
$(document).ready(function () {
    $("#showTime").hide();
    $(document).ajaxComplete(function(e, xhr, settings){
        console.log("ajaxComplete ")
        redirectHandle(xhr);
    });

    function redirectHandle(xhr) {
        //获取后台返回的参数
        var url = xhr.getResponseHeader("redirectUrl");
        console.log("redirectUrl = " + url);
        var enable = xhr.getResponseHeader("enableRedirect");
        if((enable == "true") && (url != "")){
            var win = window;
            while(win != win.top){
                win = win.top;
            }
            win.location.href = url;
        }
    }

    $("#generateCode").click(function () {
        var valEmail = $("#email").val();
        var trimEmail = valEmail.trim();
        if (valEmail == ""){
            $("#emailMsg").html("请输入邮箱地址。");
            return;
        }
        $.ajax({
            url:"../api/code/create/"+trimEmail,
            type:"GET",
            beforeSend:function () {
                $("#generateCode").hide();
                $("#showTime").show();
                var fun = function(){
                    var number = parseInt($("#showTime_left").html());
                    $("#showTime_left").html(number == 0 ? 0: number-1);
                    setTimeout(fun, 1000);
                };
                setTimeout(fun, 1000);
            },
            success:function(result,status,xhr){
                if (result == true){
                    alert("验证码已发送到您的邮箱，请查收。")
                }else {
                    alert("验证码发送失败。");
                    $("#showTime_left").html("60");
                    $("#generateCode").show();
                    $("#showTime").hide();
                }

            },
            error:function (e) {
                var res = $.parseJSON(e.responseText);
                alert("生成验证码失败："+res.message);
            }
        });
    });

    $("#btnSubmit").click(function () {
        var email = $("#email").val().trim();
        var password = $("#password").val().trim();
        var confirmPassword = $("#confirmPassword").val().trim();
        var code = $("#code").val().trim();
        if (email == ""){
            alert("请输入email");
            return ;
        }
        if (password == ""){
            alert("请输入密码");
            return ;
        }
        if (password !== confirmPassword){
            alert("确认密码不正确");
            return ;
        }
        if (code == ""){
            alert("请输入验证码。");
            return ;
        }
        $.ajax({
            url:"../api/user/register/"+email+"/"+password+"/"+code,
            type:"GET",
            success:function(result,status,xhr){
                if (!result){
                    alert("注册失败。");
                }
            },
            statusCode: {
                303:function(s) {
                    alert(s.responseText);
                },
                500:function (s) {
                    alert(s.responseText);
                }
            }
        });
    });
})
```

###### 4.7.3、欢迎界面

```javascript
$(document).ready(function () {
    // 获取指定名称的cookie
    function getCookie(name){
        var strcookie = document.cookie;//获取cookie字符串
        var arrcookie = strcookie.split("; ");//分割
        //遍历匹配
        for ( var i = 0; i < arrcookie.length; i++) {
            var arr = arrcookie[i].split("=");
            if (arr[0] == name){
                return arr[1];
            }
        }
        return "";
    }

    var token = getCookie("lagou_token");

    $.ajax({
        url:"../api/user/info"+"/"+token,
        type:"GET",
        success:function(result,status,xhr){
            if (result){
                $("#userPanel").html(result+",欢迎登陆系统。");
            }
        },
        statusCode: {
            401:function() {
                $("#userPanel").html("请登录。");
            }
        }
    });
});
```

##### 4.8、Nginx集成

```conf

#user  nobody;
worker_processes  1;

#error_log  logs/error.log;
#error_log  logs/error.log  notice;
#error_log  logs/error.log  info;

#pid        logs/nginx.pid;


events {
    worker_connections  1024;
}


http {
    include       mime.types;
    default_type  application/octet-stream;

    sendfile        on;
    #tcp_nopush     on;

    #keepalive_timeout  0;
    keepalive_timeout  65;

    #gzip  on;

    upstream myserver {
        #ip_hash;
        server 192.168.0.152:9002;
    }

    server {
        listen       80;
        server_name  localhost;
	
	location /static/ {
            root /usr/local/html/users;
        }

        location /api/ {
            proxy_pass http://myserver;
            proxy_connect_timeout 10;
        }
		
		location /favicon.ico {
			log_not_found off;
			access_log off;
		}
      
        location ~ /\.ht {
	    deny all;
        }
		
        error_page   500 502 503 504  /50x.html;
        location = /50x.html {
            root   html;
        }
    }

}
```

#### 五、系统测试

##### 5.1、用户注册

- 验证邮箱是否占用

  比如已知当前系统以及存在了350997203@qq.com的注册信息。此时点击注册时会抛出邮箱已经被注册提示。

  ![image-20200704005504756](images/image-20200704005504756.png)

- 验证码发送到邮箱

  ![image-20200704005837746](images/image-20200704005837746.png)

  输入邮箱地址，并且点击生成验证码连接，即可将验证码发送到邮箱。

  ![image-20200704010016339](images/image-20200704010016339.png)

  验证码的有效时间是10分钟。

- 用户注册

  点击【注册】按钮，将发起注册请求，注册成功之后将跳转到欢迎页。

  ![image-20200704010311021](images/image-20200704010311021.png)

- 频繁注册控制

  根据系统默认配置时1分钟能够点击2次注册动作，超过两次的则会提示操作过于频繁。

  ![image-20200704010640433](images/image-20200704010640433.png)

##### 5.2、用户登录

浏览器输入：http://www.test.com/static/login.html进入登录界面

![image-20200704010857085](images/image-20200704010857085.png)

- 用户名密码错误

  输入错误密码时给出错误提示。

  ![image-20200704010948324](images/image-20200704010948324.png)

- 登录成功

  登录成功之后重定向到welcome.html页面。

##### 5.3、欢迎登录

- 获取登录者邮箱

  如果用户已经登录，则显示邮箱地址。如果未登录或者token被清除，则提示请登录。

##### 