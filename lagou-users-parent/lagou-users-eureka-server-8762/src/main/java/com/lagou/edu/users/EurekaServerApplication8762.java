package com.lagou.edu.users;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * 开启EurekaServer功能
 */
@SpringBootApplication
@EnableEurekaServer
public class EurekaServerApplication8762 {

    public static void main(String[] args) {
        SpringApplication.run(EurekaServerApplication8762.class,args);
    }

}
