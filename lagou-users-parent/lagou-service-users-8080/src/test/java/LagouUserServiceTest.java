import com.lagou.edu.users.LagouUsersApplication8080;
import com.lagou.edu.users.pojo.LagouUser;
import com.lagou.edu.users.service.LagouUserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {LagouUsersApplication8080.class})
public class LagouUserServiceTest {
    @Autowired
    private LagouUserService lagouUserService;

    @Test
    public void testSaveLagouUser(){
        LagouUser lagouUser = new LagouUser();
        lagouUser.setEmail("350997203@qq.com");
        lagouUser.setPassword("12345");
        lagouUser.setCreatetime(new Date(System.currentTimeMillis()));
        lagouUserService.saveUser(lagouUser);
        System.out.println("save record :"+lagouUser.getId());
    }
}
