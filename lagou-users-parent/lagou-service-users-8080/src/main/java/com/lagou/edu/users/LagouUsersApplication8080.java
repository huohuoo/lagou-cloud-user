package com.lagou.edu.users;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import tk.mybatis.spring.annotation.MapperScan;

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
@MapperScan(basePackages = {"com.lagou.edu.users.mapper"})
public class LagouUsersApplication8080 {

    public static void main(String[] args) {
        SpringApplication.run(LagouUsersApplication8080.class,args);
    }

}
