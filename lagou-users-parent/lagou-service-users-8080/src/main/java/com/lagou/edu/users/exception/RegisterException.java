package com.lagou.edu.users.exception;

/**
 * 注册异常
 */
public class RegisterException extends RuntimeException {
    public RegisterException(){
        super();
    }

    public RegisterException(String msg){
        super(msg);
    }
}
