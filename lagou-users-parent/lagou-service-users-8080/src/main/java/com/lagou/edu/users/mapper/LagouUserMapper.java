package com.lagou.edu.users.mapper;

import com.lagou.edu.users.pojo.LagouUser;
import tk.mybatis.mapper.common.Mapper;

/**
 * 用户数据访问
 */
public interface LagouUserMapper extends Mapper<LagouUser> {



}
