package com.lagou.edu.users.service.fallback;

import com.lagou.edu.users.service.LagouCodeServiceFeignClient;
import org.springframework.stereotype.Component;

@Component
public class LagouCodeServiceFallback implements LagouCodeServiceFeignClient {
    /**
     * 创建验证码失败，容错
     * @param email 邮箱地址
     * @return
     */
    @Override
    public Boolean create(String email) {
        return false;
    }

    /**
     * 验证服务调用超时，容错
     * @param email 邮箱
     * @param code 验证码
     * @return
     */
    @Override
    public Integer validate(String email, String code) {
        return -1;
    }
}
