package com.lagou.edu.users.mapper;

import com.lagou.edu.users.pojo.LagouToken;
import tk.mybatis.mapper.common.Mapper;

/**
 * token数据服务
 */
public interface LagouTokenMapper extends Mapper<LagouToken> {
}
