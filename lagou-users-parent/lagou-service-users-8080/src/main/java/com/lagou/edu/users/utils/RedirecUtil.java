package com.lagou.edu.users.utils;

import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 重定向工具，
 * 处理AJAX重定向问题。
 */
@Slf4j
public class RedirecUtil {
    private RedirecUtil(){}

    /**
     *功能描述
     * 重定向处理
     */
    public static void redirect(HttpServletRequest request,HttpServletResponse response, String redirectUrl){
        try{
            //如果是Ajax请求
            if("XMLHttpRequest".equals(request.getHeader("X-Requested-With"))){
                log.debug("ajax redirect");
                sendRedirect(response,redirectUrl);
            }
            //如果是浏览器地址栏请求
            else {
                log.debug("normal redirect ");
                response.sendRedirect(redirectUrl);
            }
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }
    /**
     *功能描述
     * AJAX重定向
     */
    private static void sendRedirect(HttpServletResponse response, String redirectUrl){
        try {
            //这里并不是设置跳转页面，而是将重定向的地址发给前端，让前端执行重定向
            // 设置跳转地址
            response.setHeader("redirectUrl", redirectUrl);
            //设置跳转使能
            response.setHeader("enableRedirect","true");
            response.flushBuffer();
        } catch (IOException ex) {
            log.error("Could not redirect to: " + redirectUrl, ex);
        }
    }
}
