package com.lagou.edu.users.service;

import com.lagou.edu.users.pojo.LagouToken;
import com.lagou.edu.users.pojo.LagouUser;

/**
 * 用户服务：保存用户信息，根据查询用户信息，验证邮箱是否已经注册
 * 这里也暂时先不处理缓存问题
 */
public interface LagouUserService {

    /**
     * 保存user信息。
     * @param user
     */
    LagouUser saveUser(LagouUser user);

    /**
     * Get By Id
     *
     * @param id 主键
     * @return
     */
    LagouUser getUserById(Long id);

    /**
     * 根据邮箱获取用户信息
     *
     * @param email 邮箱
     * @return 用户信息
     */
    LagouUser getLagouUserByEmail(String email);

    /**
     * 验证邮箱是否已经注册:这里只做简单的获取统计数据，没有做用户状态控制处理
     *
     * @param email
     * @return
     */
    Integer getCheckEmail(String email);

    /**
     * 保存token信息
     *
     * @param token token
     */
    void saveToken(LagouToken token);

    /**
     * 根据email和密码生成token
     *
     * @param email email
     * @param password 密码
     * @return token字符串
     */
    String generateToken(String email,String password);

    /**
     * 根据token值获取token对象
     *
     * @param token token值
     * @return token对象
     */
    LagouToken getTokenByToken(String token);

    /**
     * 根据邮箱获取token
     * @param email
     * @return
     */
    LagouToken getTokenByEmail(String email);
}
