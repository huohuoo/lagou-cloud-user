package com.lagou.edu.users.service;

import com.lagou.edu.users.service.fallback.LagouCodeServiceFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "lagou-service-code",fallback = LagouCodeServiceFallback.class,path = "/code")
public interface LagouCodeServiceFeignClient {

    /**
     * 创建验证码
     * @param email 邮箱地址
     * @return
     */
    @GetMapping("/create/{email}")
    Boolean create(@PathVariable(name = "email") String email);


    /**
     * 校验验证码是否正确
     * @param email 邮箱
     * @param code 验证码
     * @return
     */
    @GetMapping("/validate/{email}/{code}")
    Integer validate(@PathVariable(name = "email") String email,
                            @PathVariable(name = "code") String code);

}
