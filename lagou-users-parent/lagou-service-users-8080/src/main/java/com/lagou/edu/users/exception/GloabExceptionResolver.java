package com.lagou.edu.users.exception;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;

@ControllerAdvice
public class GloabExceptionResolver {

    @ExceptionHandler(value = UserException.class)
    @ResponseBody
    public String userException(UserException ex, HttpServletResponse response){
        response.setStatus(500);
        return ex.getMessage();
    }

    @ExceptionHandler(value = RegisterException.class)
    @ResponseBody
    public String registerException(RegisterException ex , HttpServletResponse response){
        response.setStatus(500);
        return ex.getMessage();
    }

}
