package com.lagou.edu.email.service;

/**
 * 发送邮件服务
 */
public interface MailService {

    /**
     * 发送
     * @param to 目标
     * @param subject 主题
     * @param text 内容
     */
    void send(String to, String subject, String text);

}
