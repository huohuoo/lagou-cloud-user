package com.lagou.edu.email.controller;

import com.lagou.edu.email.service.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * 发送邮件
 */
@RestController
public class EmailController {

    @Autowired
    private MailService mailService;

    /**
     * 发送验证码到邮件
     * @param email 邮箱地址
     * @param code 验证码
     * @return true成功，false失败
     */
    @GetMapping("/email/{email}/{code}")
    public Boolean email(@PathVariable(name = "email") String email,
                         @PathVariable(name = "code") String code){
        try {
            mailService.send(email,"拉钩验证码",code);
            return true;
        }catch (Exception e){
            return false;
        }
    }

}
