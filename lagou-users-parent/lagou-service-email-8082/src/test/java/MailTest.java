import com.lagou.edu.email.EmailApplication8082;
import com.lagou.edu.email.service.MailService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {EmailApplication8082.class})
public class MailTest {
    @Autowired
    private MailService mailService;

    @Test
    public void testSend(){
        long start = System.currentTimeMillis();
        mailService.send("350997203@qq.com","Lagou验证码","221122");
        System.out.println("use time :" + (System.currentTimeMillis()-start));
    }

}
