import com.lagou.edu.code.CodeApplication8081;
import com.lagou.edu.code.pojo.LagouAuthCodeParam;
import com.lagou.edu.code.service.LagouAuthCodeService;
import com.lagou.edu.users.pojo.LagouAuthCode;
import org.apache.commons.lang.time.DateUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.text.ParseException;
import java.util.Date;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {CodeApplication8081.class})
public class LagouAuthServiceTest {
    @Autowired
    private LagouAuthCodeService lagouAuthCodeService;

    @Test
    public void testSaveLagouCode(){
        LagouAuthCode code = new LagouAuthCode();
        code.setEmail("350997203@qq.com");
        code.setCode(lagouAuthCodeService.generateCode());
        lagouAuthCodeService.saveNewAuthCode(code);
        System.out.println("save record :"+code.getId());
    }

    @Test
    public void testValidateCode() throws ParseException {
        LagouAuthCodeParam param = new LagouAuthCodeParam();
        param.setEmail("350997203@qq.com");
        Date date = DateUtils.parseDate("2020-06-28 15:27:42", new String[]{"yyyy-MM-dd HH:mm:ss"});
        param.setCurrentTime(date);
        LagouAuthCode authCodeByEmailAndTime = lagouAuthCodeService.getAuthCodeByEmailAndTime(param);
        System.out.println("code is："+authCodeByEmailAndTime.getCode());
    }
}
