package com.lagou.edu.code.mapper;

import com.lagou.edu.code.pojo.LagouAuthCodeParam;
import com.lagou.edu.users.pojo.LagouAuthCode;
import tk.mybatis.mapper.common.Mapper;

/**
 * 验证码用户数据访问
 */
public interface LagouAuthCodeMapper extends Mapper<LagouAuthCode> {

    /**
     * 查询邮箱当前有效的验证码
     * @param email
     * @return
     */
    LagouAuthCode getAuthCodeByEmailInCurrent(LagouAuthCodeParam email);

}
