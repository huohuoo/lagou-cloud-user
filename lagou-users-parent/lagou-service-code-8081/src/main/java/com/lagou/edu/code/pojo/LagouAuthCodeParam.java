package com.lagou.edu.code.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class LagouAuthCodeParam {
    private String email;
    private Date currentTime;
}
