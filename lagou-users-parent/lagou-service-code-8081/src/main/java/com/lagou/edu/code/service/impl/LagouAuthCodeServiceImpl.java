package com.lagou.edu.code.service.impl;

import com.lagou.edu.code.mapper.LagouAuthCodeMapper;
import com.lagou.edu.code.pojo.LagouAuthCodeParam;
import com.lagou.edu.code.service.LagouAuthCodeService;
import com.lagou.edu.users.pojo.LagouAuthCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class LagouAuthCodeServiceImpl implements LagouAuthCodeService {

    @Autowired
    private LagouAuthCodeMapper lagouAuthCodeMapper;

    @Override
    public LagouAuthCode getAuthCodeByEmailAndTime(LagouAuthCodeParam param) {
        LagouAuthCode authCodeByEmailInCurrent = lagouAuthCodeMapper.getAuthCodeByEmailInCurrent(param);
        return authCodeByEmailInCurrent;
    }

    @Override
    public void saveNewAuthCode(LagouAuthCode code) {
        code.setCreatetime(new Date(System.currentTimeMillis()));
        code.setExpiretime(new Date(System.currentTimeMillis()+10*60*1000));
//        code.setCode(""+(int)((Math.random()*9+1)*100000));
        lagouAuthCodeMapper.insert(code);
    }

    @Override
    public String generateCode() {
        return ""+(int)((Math.random()*9+1)*1000);
    }
}
