package com.lagou.edu.code;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import tk.mybatis.spring.annotation.MapperScan;

@SpringBootApplication
@EnableDiscoveryClient
@MapperScan(basePackages = {"com.lagou.edu.code.mapper"})
@EnableFeignClients
public class CodeApplication8081 {

    public static void main(String[] args) {
        SpringApplication.run(CodeApplication8081.class,args);
    }

}
