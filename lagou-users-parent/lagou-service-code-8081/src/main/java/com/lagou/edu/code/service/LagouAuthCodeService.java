package com.lagou.edu.code.service;

import com.lagou.edu.code.pojo.LagouAuthCodeParam;
import com.lagou.edu.users.pojo.LagouAuthCode;

/**
 * 验证码业务
 */
public interface LagouAuthCodeService {

    /**
     * 根据email获取当前时间的验证码
     * @param param email地址
     * @return 验证码
     */
    LagouAuthCode getAuthCodeByEmailAndTime(LagouAuthCodeParam param);

    /**
     * 创建验证码
     *
     * @return 验证码
     */
     void saveNewAuthCode(LagouAuthCode code);

    /**
     *
     * @return
     */
    String generateCode();

}
