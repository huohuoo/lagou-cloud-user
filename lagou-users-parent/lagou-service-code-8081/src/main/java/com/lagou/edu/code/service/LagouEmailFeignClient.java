package com.lagou.edu.code.service;

import com.lagou.edu.code.service.fallback.LagouEmailFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * 邮件服务Feign客户端
 */
@FeignClient(value = "lagou-service-email",fallback = LagouEmailFallback.class,path = "/")
public interface LagouEmailFeignClient {

    @GetMapping("/email/{email}/{code}")
    Boolean email(@PathVariable(name = "email") String email,
                  @PathVariable(name = "code") String code);
}
