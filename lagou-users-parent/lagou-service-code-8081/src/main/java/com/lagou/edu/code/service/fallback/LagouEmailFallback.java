package com.lagou.edu.code.service.fallback;

import com.lagou.edu.code.service.LagouEmailFeignClient;
import org.springframework.stereotype.Component;

/**
 * Histrix熔断降级
 */
@Component
public class LagouEmailFallback implements LagouEmailFeignClient {
    @Override
    public Boolean email(String email, String code) {
        return false;
    }
}
