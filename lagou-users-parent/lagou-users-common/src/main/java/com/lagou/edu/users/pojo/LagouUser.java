package com.lagou.edu.users.pojo;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Table(name = "lagou_users")
public class LagouUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id; //主键
    private String username; //用户名
    private String password; //密码
    private String email;    //邮箱
    private Date createtime; //创建时间
}
