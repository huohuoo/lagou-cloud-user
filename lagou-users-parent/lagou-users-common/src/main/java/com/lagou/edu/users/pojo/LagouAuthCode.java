package com.lagou.edu.users.pojo;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Table(name = "lagou_auth_code")
public class LagouAuthCode {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;   //自增主键
    private String email; //邮箱地址
    private String code;  //验证码
    private Date createtime; //创建时间
    private Date expiretime; //过期时间
}
