package com.lagou.edu.users.pojo;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "lagou_token")
public class LagouToken {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;   //自增主键
    private String email; //邮箱地址
    private String token; //令牌
}
