package com.lagou.edu.users.service.fallback;

import com.lagou.edu.users.service.LagouUsersFeignClient;
import org.springframework.stereotype.Component;

@Component
public class LagouUsersFallback implements LagouUsersFeignClient {
    @Override
    public String info(String token) {
        return null;
    }
}
