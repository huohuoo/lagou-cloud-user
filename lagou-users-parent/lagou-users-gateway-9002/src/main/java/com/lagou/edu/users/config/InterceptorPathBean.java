package com.lagou.edu.users.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

@Data
@Component
@ConfigurationProperties(prefix = "interceptorconfig.path")
public class InterceptorPathBean {
    /**
     * 需要拦截的路径。
     */
    private List<String> include;

    /**
     * 排除需要拦截的地址
     */
    private List<String> exclude;

}
