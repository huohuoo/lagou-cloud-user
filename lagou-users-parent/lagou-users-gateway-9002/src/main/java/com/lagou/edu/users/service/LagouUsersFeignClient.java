package com.lagou.edu.users.service;

import com.lagou.edu.users.service.fallback.LagouUsersFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "lagou-service-users",fallback = LagouUsersFallback.class,path = "/user")
public interface LagouUsersFeignClient {

    @GetMapping("/info/{token}")
    String info(@PathVariable("token") String token);

}
